import * as React from 'react';
import { State } from 'wyn-broadcaster';
import { Component } from './Component';

export function createContext<T>(state: State<T>) {
  const context = React.createContext(state.value);
  const Provider = createProvider<T>(state, context);
  const { Consumer } = context;

  return {
    Provider,
    Consumer,
    setState(values: Partial<T>) {
      state.set(values);
    }
  };
}

function createProvider<T>(state: State<T>, context: React.Context<T>) {
  return class Provider<T> extends Component {
    state = state.value;

    componentWillMount() {
      super.componentWillMount();
      this.subscribe(state, this.rerender);
    }

    render() {
      const { props, state } = this;
      const { Provider } = context;

      return (
        <Provider value={state}>
          {props.children}
        </Provider>
      );
    }

    rerender = state => this.setState(state);
  }
}
