import { Component as ReactComponent } from 'react';

import { Broadcaster } from 'wyn-broadcaster';

import {
  Subscriber,
  Subscription,
  StartSubscriptions
} from '../typings/typings';


export class Component<P = {}, S = {}> extends ReactComponent<P, S> {
  private subscriptions = new Map<number, Subscription<any>>();

  protected onMountSubscriptions: StartSubscriptions = [];

  private defaultSubscriber = () => {
    this.forceUpdate();
  }

  componentWillMount() {
    if (super.componentWillMount) {
      super.componentWillMount();
    }

    for (let item of this.onMountSubscriptions) {
      if (Array.isArray(item)) {
        let [broadcaster, subscriber] = item;
        this.subscribe(broadcaster, subscriber);
      } else {
        this.subscribe(item);
      }
    }
  }

  subscribe<T>(broadcaster: Broadcaster<T>, func: Subscriber<T> = this.defaultSubscriber) {
    if (this.subscriptions.has(broadcaster.id)) {
      return;
    }

    var id = broadcaster.subscribe(func);
    this.subscriptions.set(broadcaster.id, { id, broadcaster });
  }

  componentWillUnmount() {
    if (super.componentWillUnmount) {
      super.componentWillUnmount();
    }

    this.subscriptions.forEach(subscription => {
      subscription.broadcaster.unsubscribe(subscription.id);
    });
    this.subscriptions.clear();
  }

  async asyncSetState<K extends keyof S>(
    state: ((prevState: Readonly<S>, props: P) => (Pick<S, K> | S | null)) | (Pick<S, K> | S | null)
  ) {
    return new Promise<void>(resolve => {
      this.setState(state, resolve);
    });
  }

  async asyncForceUpdate() {
    return new Promise<void>(resolve => {
      this.forceUpdate(resolve);
    });
  }

}
