const dist = require('./dist/main');

if (typeof exports !== 'undefined') {
  exports.Component = dist.Component;
  exports.createContext = dist.createContext;
}

if (typeof window !== 'undefined') {
  if (typeof define === 'function' && define['amd']) {
    define(() => dist);
  }
}
