# wyn-component

This package requires [wyn-broadcaster](https://www.npmjs.com/package/wyn-broadcaster).

## Usage Example

```jsx
const React = require('react')
const { Action } = require('wyn-broadcaster')
const Component = require('wyn-component')

const exampleAction = new Action()

class ExampleComponent extends Component {
    componentWillMount() {
        this.subscribe(exampleAction, function(value)) {
            // what to do when the action broadcasts a value
        }
    }
}
```
Components will automatically unsubscribe to broadcasters as they unmount.
