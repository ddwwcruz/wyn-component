import { Broadcaster } from 'wyn-broadcaster';

export interface Subscription<T> {
  id: number;
  broadcaster: Broadcaster<T>;
}

export type Subscriber<T> = (args: T) => any;
export type StartSubscription = Broadcaster<any> | [Broadcaster<any>, Subscriber<any>];
export type StartSubscriptions = StartSubscription[];
